const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";
// *[SECTION] JSON web token
// *Token creation
/*  
    *Analogy:
        *Pack the gift provided with a lock, which can only be opened using the secret code as the key.
*/


//Token creation
module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	// *generate a JSON web token using the jwt's sign method
    // *Syntax
    /* 
        *jwt.sign(payload, secretOrPrivateKey,[callbackFunction])
    */
	//jwt.sign(data/payload, secretkey, options);
	return jwt.sign(data, secret, {});

}

// Token Verification
// *Analogy- receive the gift and open the lock to verify if the sender is legitimate and the gift is not tampered with

module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization;
    if(typeof token !== "undefined") {
    	console.log(token);
    // *validate the "token" using the verify method, the decrypt the token using the secret code
    /* 
        *Syntax:
            *jwt.verify(token,secret,[callbackFunction])
    */
        token = token.slice(7, token.length);
        return jwt.verify(token, secret, (error, data) => {
            if(error) {
                return res.send({auth: "failed" })
            } else {
                next();
            }
    	})

    } else {
        return res.send({auth: "failed" })
	}
}

// Token decryption
/* 
*Analogy:
*opening a gift or unwrapping a present
*/
module.exports.decode = (token) => {
    if (typeof token !== "undefined") {
       token = token.slice(7,token.length);

        return jwt.verify(token, secret,(error, data) => {
            if(error) {
                return null
            } else {
                // *decode method is used to obtain the info from the JWT
                // *Syntax: jwt.decode(token,[options])
                // *it will return an obj with the access to the payload property
                return jwt.decode(token, {complete:true}).payload;
            }
        })

    } else {
    	return null
    }
}


const express = require('express');
const mongoose = require('mongoose');
const cors = require("cors");
const app = express();
const port = 4000;
const userRoute = require("./routes/userRoute")
const courseRoute = require("./routes/courseRoute")

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


mongoose.set('strictQuery', false);
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.wqnqsck.mongodb.net/B241-API?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the Atlas!"));

app.use("/users", userRoute);
app.use("/courses", courseRoute);

app.listen(port, () => console.log(`Now listening to port ${port}`));

